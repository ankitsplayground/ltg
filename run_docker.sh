#!/bin/bash

echo "Building application jar..."
./gradlew clean build

echo "Building docker image..."
docker build --build-arg JAR_FILE=build/libs/*.jar -t ltg/checkoutservice .

echo "Running docker image..."
docker run -p8080:8080 ltg/checkoutservice