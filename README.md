# Introduction

CheckoutService is an API to checkout a shopping cart containing apples and oranges. This service provides a REST
interface to calculate total price for items in the shopping cart.

## Technologies

1. Programming Language - Java8
2. Application Framework - Spring Boot

## Building and Running

### Build

```
./gradlew clean build
```

### Run

```
./gradlew bootRun
```

### Using Docker to Run the Application

To run the application using Docker, use the following command:

```
./run_docker.sh
```

This script does the following:
1. Do a clean build.
```
./gradlew clean build
```
2. Build Docker image.
```
docker build --build-arg JAR_FILE=build/libs/*.jar -t ltg/checkoutservice .
```
3. Run the docker image.
```
docker run -p8080:8080 ltg/checkoutservice
```

## Accessing the API

### Request

```
curl -X POST \
  http://localhost:8080/api/checkout/1.0/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
	"items": ["apple", "orange", "apple", "orange", "apple", "apple"]
}'
```

### Response

```
{
    "items": [
        {
            "name": "apple",
            "price": 0.6
        },
        {
            "name": "orange",
            "price": 0.25
        },
        {
            "name": "apple",
            "price": 0.6
        },
        {
            "name": "orange",
            "price": 0.25
        },
        {
            "name": "apple",
            "price": 0.6
        },
        {
            "name": "apple",
            "price": 0.6
        }
    ],
    "totalPrice": 1.7,
    "offersApplied": [
        {
            "discount": 1.2,
            "offerDescription": "Buy One Get One Apple Free"
        },
        {
            "discount": 0,
            "offerDescription": "3 for the price of 2 Oranges"
        }
    ]
}
```