package com.ltg.applesnoranges.checkoutservice.offer;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.vo.OfferResult;

import java.util.List;

/**
 * Offer is an interface to apply promotions such as <i>buy one get one free</i> to the items in a shopping cart.
 * It defines a method <i>apply</i> which returns a discount based on the promotion.
 */
public interface Offer {

    /**
     * Apply the offer and return the discount based on the promotion.
     * @param items List of items
     * @return OfferResult containing offer description and discount.
     */
    OfferResult apply(List<Item> items);

    String getDescription();
}
