package com.ltg.applesnoranges.checkoutservice.offer;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.util.ItemFactory;
import com.ltg.applesnoranges.checkoutservice.vo.OfferResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Offer implementation to support Buy One Get One Free Apple promotion.
 */
public class BuyOneGetOneFreeAppleOffer implements Offer {

    /**
     * Apply the Buy One Get One Free Apple promotion.
     * @param items List of items
     * @return Double discount.
     */
    @Override
    public OfferResult apply(List<Item> items) {
        Item apple = ItemFactory.from("apple");
        List<Item> apples = items.stream().filter(i -> apple.getName().equals(i.getName())).collect(Collectors.toList());
        Double discount = (apples.size()/2) *  apple.getPrice();
        return new OfferResult(discount, getDescription());
    }

    /**
     * Get Offer Description.
     * @return String offer description.
     */
    @Override
    public String getDescription() {
        return "Buy One Get One Apple Free";
    }
}
