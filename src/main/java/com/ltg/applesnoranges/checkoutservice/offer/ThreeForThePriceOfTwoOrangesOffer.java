package com.ltg.applesnoranges.checkoutservice.offer;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.util.ItemFactory;
import com.ltg.applesnoranges.checkoutservice.vo.OfferResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Offer implementation to support 3 for the price of 2 oranges promotion.
 */
public class ThreeForThePriceOfTwoOrangesOffer implements Offer {

    /**
     * Apply the 3 for the price of 2 oranges promotion.
     * @param items List of items
     * @return OfferResult offer result containing discount and offer description.
     */
    @Override
    public OfferResult apply(List<Item> items) {
        Item orange = ItemFactory.from("orange");
        List<Item> oranges = items.stream().filter(i -> orange.getName().equals(i.getName())).collect(Collectors.toList());
        Double discount = (oranges.size()/3) *  orange.getPrice();
        return new OfferResult(discount, getDescription());
    }

    /**
     * Get Offer Description.
     * @return String offer description.
     */
    @Override
    public String getDescription() {
        return "3 for the price of 2 Oranges";
    }
}
