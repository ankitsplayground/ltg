package com.ltg.applesnoranges.checkoutservice.service;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.util.ItemFactory;
import com.ltg.applesnoranges.checkoutservice.util.PriceCalculator;
import com.ltg.applesnoranges.checkoutservice.vo.CheckoutResponse;
import com.ltg.applesnoranges.checkoutservice.vo.ShoppingCart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * Implementation of CheckoutService interface to calculate total price of items in a shopping cart.
 */
@Service
public class CheckoutServiceImpl implements CheckoutService {
    private static Logger logger = LoggerFactory.getLogger(CheckoutServiceImpl.class);

    private PriceCalculator priceCalculator;

    @Autowired
    public CheckoutServiceImpl(PriceCalculator priceCalculator) {
        this.priceCalculator = priceCalculator;
    }

    /**
     * Checkout method of calculate the price of all items in a shopping cart.
     * @param shoppingCart shopping cart containing an array of item names.
     * @return CheckoutResponse containing the list of items, offers applied, and the total price.
     */
    @Override
    public CheckoutResponse checkout(ShoppingCart shoppingCart) {
        String[] shoppingCartItems = shoppingCart.getItems();
        if(shoppingCartItems == null || shoppingCartItems.length == 0) {
            throw new IllegalArgumentException("No items in the cart.");
        }
        List<Item> items = new ArrayList<>();
        Stream.of(shoppingCartItems).forEach(i -> items.add(ItemFactory.from(i)));
        return priceCalculator.calculatePrice(Collections.unmodifiableList(items));
    }
}
