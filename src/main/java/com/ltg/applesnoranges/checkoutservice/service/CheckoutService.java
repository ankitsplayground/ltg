package com.ltg.applesnoranges.checkoutservice.service;

import com.ltg.applesnoranges.checkoutservice.vo.CheckoutResponse;
import com.ltg.applesnoranges.checkoutservice.vo.ShoppingCart;

public interface CheckoutService {

    CheckoutResponse checkout(ShoppingCart shoppingCart);
}
