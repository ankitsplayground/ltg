package com.ltg.applesnoranges.checkoutservice.controller;

import com.ltg.applesnoranges.checkoutservice.service.CheckoutService;
import com.ltg.applesnoranges.checkoutservice.vo.CheckoutResponse;
import com.ltg.applesnoranges.checkoutservice.vo.ShoppingCart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller class to handle the incoming requests to perform checkout operation.
 */
@Controller
@RequestMapping("/api/checkout/1.0")
public class CheckoutController {
    private static final Logger logger = LoggerFactory.getLogger(CheckoutController.class);

    private CheckoutService checkoutService;

    @Autowired
    public CheckoutController(CheckoutService checkoutService) {
        this.checkoutService = checkoutService;
    }

    /**
     * Method accepting POST requests to calculate the total price of items in a shopping cart.
     * @param shoppingCart contains a list of item names.
     * @return ResponseEntity wraps CheckoutResponse object containing the items in the list, their price and the total price.
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CheckoutResponse> checkout(@RequestBody ShoppingCart shoppingCart) {
        CheckoutResponse checkoutResponse = checkoutService.checkout(shoppingCart);
        return new ResponseEntity<>(checkoutResponse, HttpStatus.OK);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity handleIllegalItems(IllegalArgumentException illegalArgumentException) {
        logger.error(illegalArgumentException.getMessage(), illegalArgumentException);
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

}
