package com.ltg.applesnoranges.checkoutservice.vo;

import com.ltg.applesnoranges.checkoutservice.domain.Item;

import java.util.List;

public class CheckoutResponse {

    private List<Item> items;
    private Double totalPrice;
    private List<OfferResult> offersApplied;

    public CheckoutResponse(List<Item> items, Double totalPrice, List<OfferResult> offersApplied) {
        this.items = items;
        this.totalPrice = totalPrice;
        this.offersApplied = offersApplied;
    }

    public List<Item> getItems() {
        return items;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public List<OfferResult> getOffersApplied() {
        return offersApplied;
    }
}
