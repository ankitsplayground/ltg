package com.ltg.applesnoranges.checkoutservice.vo;

public class OfferResult {

    private Double discount;
    private String offerDescription;

    public OfferResult(Double discount, String offerDescription) {
        this.discount = discount;
        this.offerDescription = offerDescription;
    }

    public Double getDiscount() {
        return discount;
    }

    public String getOfferDescription() {
        return offerDescription;
    }
}
