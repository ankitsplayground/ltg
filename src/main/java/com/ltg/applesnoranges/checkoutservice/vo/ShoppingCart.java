package com.ltg.applesnoranges.checkoutservice.vo;

public class ShoppingCart {

    private String[] items;

    public String[] getItems() {
        return items;
    }

    public void setItems(String[] items) {
        this.items = items;
    }
}
