package com.ltg.applesnoranges.checkoutservice.config;

import com.ltg.applesnoranges.checkoutservice.offer.BuyOneGetOneFreeAppleOffer;
import com.ltg.applesnoranges.checkoutservice.offer.ThreeForThePriceOfTwoOrangesOffer;
import com.ltg.applesnoranges.checkoutservice.util.PriceCalculator;
import com.ltg.applesnoranges.checkoutservice.util.PriceCalculatorImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Main configuration class for the Checkout Service.
 */
@Configuration
public class CheckoutServiceConfig {


    @Bean
    public PriceCalculator priceCalculator() {
        PriceCalculator priceCalculator  = new PriceCalculatorImpl();
        priceCalculator.addOffer(new BuyOneGetOneFreeAppleOffer());
        priceCalculator.addOffer(new ThreeForThePriceOfTwoOrangesOffer());
        return priceCalculator;
    }
}
