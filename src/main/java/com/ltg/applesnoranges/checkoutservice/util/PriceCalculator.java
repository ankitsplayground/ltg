package com.ltg.applesnoranges.checkoutservice.util;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.offer.Offer;
import com.ltg.applesnoranges.checkoutservice.vo.CheckoutResponse;

import java.util.List;

public interface PriceCalculator {

    CheckoutResponse calculatePrice(List<Item> items);

    void addOffer(Offer offers);
}
