package com.ltg.applesnoranges.checkoutservice.util;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Factory to get items.
 */
public class ItemFactory {
    private static final Logger logger = LoggerFactory.getLogger(ItemFactory.class);

    private ItemFactory() {

    }

    /**
     * Factory method to get the item object from the name.
     * @param name String item name.
     * @return Item if name is a valid item, null otherwise.
     */
    public static Item from(String name) {
        logger.debug("Finding item for {}", name);
        if("apple".equalsIgnoreCase(name)) {
            return new Item("apple", Double.valueOf("0.6"));
        }
        if("orange".equalsIgnoreCase(name)) {
            return new Item("orange", Double.valueOf("0.25"));
        }

        logger.warn("Invalid item name {} passed", name);
        throw new IllegalArgumentException("Item " + name + " not recognized.");
    }
}
