package com.ltg.applesnoranges.checkoutservice.util;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.offer.Offer;
import com.ltg.applesnoranges.checkoutservice.vo.CheckoutResponse;
import com.ltg.applesnoranges.checkoutservice.vo.OfferResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class PriceCalculatorImpl implements PriceCalculator {
    private static  final Logger logger = LoggerFactory.getLogger(PriceCalculatorImpl.class);

    private List<Offer> offers;

    public PriceCalculatorImpl() {
        offers = new ArrayList<>();
    }

    @Override
    public CheckoutResponse calculatePrice(List<Item> items) {
        logger.info("Calculating price for {} items", items.size());
        List<OfferResult> offersApplied = new ArrayList<>();
        Double totalPrice = items.stream().mapToDouble(Item::getPrice).sum();
        for(Offer offer : offers) {
            logger.info("Applying offer {}", offer.getDescription());
            OfferResult offerResult = offer.apply(items);
            totalPrice -= offerResult.getDiscount();
            offersApplied.add(offerResult);
        }
        return new CheckoutResponse(items, totalPrice, offersApplied);
    }

    @Override
    public synchronized void addOffer(Offer offer) {
        logger.info("Adding offer {}", offer.getDescription());
        offers.add(offer);
    }
}
