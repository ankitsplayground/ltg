package com.ltg.applesnoranges.checkoutservice.offer;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.util.ItemFactory;
import com.ltg.applesnoranges.checkoutservice.vo.OfferResult;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ThreeForThePriceOfTwoOrangesOfferTest {

    private Offer offer;
    private static final String OFFER_DESCRIPTION = "3 for the price of 2 Oranges";

    @Before
    public void setUp() {
        offer = new ThreeForThePriceOfTwoOrangesOffer();
    }

    @After
    public void tearDown() {
        offer = null;
    }

    @Test
    public void doNotApplyDiscountForLessThanThreeOrangesTest() {
        Item orange = ItemFactory.from("orange");
        List<Item> items = Arrays.asList(orange, orange);

        OfferResult offerResult = offer.apply(items);
        Assert.assertNotNull(offerResult);
        Assert.assertEquals(OFFER_DESCRIPTION, offerResult.getOfferDescription());
        Assert.assertEquals(Double.valueOf("0"), offerResult.getDiscount());
    }

    @Test
    public void applyBuyThreeForThePriceOfTwoOrangesSimpleTest() {
        Item orange = ItemFactory.from("orange");
        List<Item> items = Arrays.asList(orange, orange, orange);

        OfferResult offerResult = offer.apply(items);
        Assert.assertNotNull(offerResult);
        Assert.assertEquals(OFFER_DESCRIPTION, offerResult.getOfferDescription());
        Assert.assertEquals(orange.getPrice(), offerResult.getDiscount());
    }

    @Test
    public void applyBuyThreeForThePriceOfTwoOrangesApplesAndOrangesTest() {
        Item apple = ItemFactory.from("apple");
        Item orange = ItemFactory.from("orange");
        List<Item> items = Arrays.asList(apple, apple, orange, orange, orange, orange, orange, orange);

        OfferResult offerResult = offer.apply(items);
        Assert.assertNotNull(offerResult);
        Assert.assertEquals(OFFER_DESCRIPTION, offerResult.getOfferDescription());
        Assert.assertEquals(orange.getPrice() * 2, offerResult.getDiscount(), 0.0);
    }

    @Test
    public void applyBuyThreeForThePriceOfTwoOrangesNoOrangeTest() {
        Item apple = ItemFactory.from("apple");
        List<Item> items = Arrays.asList(apple, apple);

        OfferResult offerResult = offer.apply(items);
        Assert.assertNotNull(offerResult);
        Assert.assertEquals(OFFER_DESCRIPTION, offerResult.getOfferDescription());
        Assert.assertEquals(0, offerResult.getDiscount(), 0.0);
    }


}
