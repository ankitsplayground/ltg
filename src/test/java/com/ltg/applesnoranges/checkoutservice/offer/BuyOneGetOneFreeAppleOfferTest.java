package com.ltg.applesnoranges.checkoutservice.offer;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.util.ItemFactory;
import com.ltg.applesnoranges.checkoutservice.vo.OfferResult;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class BuyOneGetOneFreeAppleOfferTest {

    private static final String OFFER_DESCRIPTION = "Buy One Get One Apple Free";
    private Offer offer;

    @Before
    public void setUp() {
        offer = new BuyOneGetOneFreeAppleOffer();
    }

    @After
    public void tearDown() {
        offer = null;
    }

    @Test
    public void doNotapplyDiscountForOneAppleTest() {
        Item apple = ItemFactory.from("apple");
        List<Item> items = Arrays.asList(apple);

        OfferResult offerResult = offer.apply(items);
        Assert.assertNotNull(offerResult);
        Assert.assertEquals(OFFER_DESCRIPTION, offerResult.getOfferDescription());
        Assert.assertEquals(Double.valueOf("0"), offerResult.getDiscount());
    }

    @Test
    public void applyBuyOneGetOneFreeAppleSimpleTest() {
        Item apple = ItemFactory.from("apple");
        List<Item> items = Arrays.asList(apple, apple);

        OfferResult offerResult = offer.apply(items);
        Assert.assertNotNull(offerResult);
        Assert.assertEquals(OFFER_DESCRIPTION, offerResult.getOfferDescription());
        Assert.assertEquals(apple.getPrice(), offerResult.getDiscount());
    }

    @Test
    public void applyBuyOneGetOneFreeAppleApplesAndOrangesListTest() {
        Item apple = ItemFactory.from("apple");
        Item orange = ItemFactory.from("orange");
        List<Item> items = Arrays.asList(apple, apple, apple, apple, apple, apple, orange, orange);

        OfferResult offerResult = offer.apply(items);
        Assert.assertNotNull(offerResult);
        Assert.assertEquals(OFFER_DESCRIPTION, offerResult.getOfferDescription());
        Assert.assertEquals(apple.getPrice() * 3, offerResult.getDiscount(), 0.0);
    }

    @Test
    public void applyBuyOneGetOneFreeAppleNoAppleTest() {
        Item orange = ItemFactory.from("orange");
        List<Item> items = Arrays.asList(orange, orange);

        OfferResult offerResult = offer.apply(items);
        Assert.assertNotNull(offerResult);
        Assert.assertEquals(OFFER_DESCRIPTION, offerResult.getOfferDescription());
        Assert.assertEquals(0, offerResult.getDiscount(), 0.0);
    }


}
