package com.ltg.applesnoranges.checkoutservice.service;

import com.ltg.applesnoranges.checkoutservice.util.ItemFactory;
import com.ltg.applesnoranges.checkoutservice.util.PriceCalculatorImpl;
import com.ltg.applesnoranges.checkoutservice.vo.CheckoutResponse;
import com.ltg.applesnoranges.checkoutservice.vo.ShoppingCart;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CheckoutServiceTest {

    private CheckoutService checkoutService;

    @Before
    public void setUp() {
        checkoutService = new CheckoutServiceImpl(new PriceCalculatorImpl());
    }

    @After
    public void tearDown() {
        checkoutService = null;
    }


    @Test
    public void appleCheckoutTest() throws Exception {
        final String item = "apple";
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setItems(new String[]{item});
        CheckoutResponse checkoutResponse = checkoutService.checkout(shoppingCart);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(1, checkoutResponse.getItems().size());
        Assert.assertEquals(((ItemFactory.from(item).getPrice())* shoppingCart.getItems().length),
                checkoutResponse.getTotalPrice(), 0.0);
    }

    @Test
    public void applesCheckoutTest() throws Exception {
        final String item = "apple";
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setItems(new String[]{item, item});
        CheckoutResponse checkoutResponse = checkoutService.checkout(shoppingCart);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(2, checkoutResponse.getItems().size());
        Assert.assertEquals(((ItemFactory.from(item).getPrice())* shoppingCart.getItems().length),
                checkoutResponse.getTotalPrice(), 0.0);
    }

    @Test
    public void orangeCheckoutTest() throws Exception {
        final String item = "orange";
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setItems(new String[]{item});
        CheckoutResponse checkoutResponse = checkoutService.checkout(shoppingCart);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(1, checkoutResponse.getItems().size());
        Assert.assertEquals(((ItemFactory.from(item).getPrice())* shoppingCart.getItems().length),
                checkoutResponse.getTotalPrice(), 0.0);
    }

    @Test
    public void orangesCheckoutTest() throws Exception {
        final String item = "orange";
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setItems(new String[]{item, item});
        CheckoutResponse checkoutResponse = checkoutService.checkout(shoppingCart);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(2, checkoutResponse.getItems().size());
        Assert.assertEquals(((ItemFactory.from(item).getPrice())* shoppingCart.getItems().length),
                checkoutResponse.getTotalPrice(), 0.0);
    }

    @Test
    public void appleAndOrangeCheckoutTest() throws Exception {
        final String apple = "apple";
        final String orange = "orange";
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setItems(new String[]{apple, orange});
        CheckoutResponse checkoutResponse = checkoutService.checkout(shoppingCart);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(2, checkoutResponse.getItems().size());
        Assert.assertEquals(Double.valueOf("0.85"), checkoutResponse.getTotalPrice());
    }

    @Test
    public void applesAndOrangesCheckoutTest() throws Exception {
        final String apple = "apple";
        final String orange = "orange";
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setItems(new String[]{apple, orange, apple, orange});
        CheckoutResponse checkoutResponse = checkoutService.checkout(shoppingCart);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(4, checkoutResponse.getItems().size());
        Assert.assertEquals(((ItemFactory.from(apple).getPrice() * 2) + (ItemFactory.from(orange).getPrice() * 2)),
                checkoutResponse.getTotalPrice(), 0.0);
    }

}
