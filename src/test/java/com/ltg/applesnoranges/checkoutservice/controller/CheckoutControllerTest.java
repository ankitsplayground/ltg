package com.ltg.applesnoranges.checkoutservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.offer.BuyOneGetOneFreeAppleOffer;
import com.ltg.applesnoranges.checkoutservice.offer.ThreeForThePriceOfTwoOrangesOffer;
import com.ltg.applesnoranges.checkoutservice.util.ItemFactory;
import com.ltg.applesnoranges.checkoutservice.vo.CheckoutResponse;
import com.ltg.applesnoranges.checkoutservice.vo.OfferResult;
import com.ltg.applesnoranges.checkoutservice.vo.ShoppingCart;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CheckoutControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void simpleCheckoutTest() throws Exception {
        ShoppingCart testShoppingCart = new ShoppingCart();
        testShoppingCart.setItems(new String[]{"apple", "orange"});
        Item apple = ItemFactory.from("apple");
        Item orange = ItemFactory.from("orange");
        List<OfferResult> offersApplied = Arrays.asList(
                new OfferResult(Double.valueOf("0"), new BuyOneGetOneFreeAppleOffer().getDescription()),
                new OfferResult(Double.valueOf("0"), new ThreeForThePriceOfTwoOrangesOffer().getDescription()));
        CheckoutResponse checkoutResponse = new CheckoutResponse(Arrays.asList(apple, orange),
                apple.getPrice() + orange.getPrice(), offersApplied);
        ObjectMapper jsonMapper = new ObjectMapper();
        this.mockMvc.perform(
                post("/api/checkout/1.0")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(testShoppingCart)))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonMapper.writeValueAsString(checkoutResponse)));
    }

    @Test
    public void buyOneGetOneAppleCheckoutTest() throws Exception {
        ShoppingCart testShoppingCart = new ShoppingCart();
        testShoppingCart.setItems(new String[]{"apple", "apple", "orange"});
        Item apple = ItemFactory.from("apple");
        Item orange = ItemFactory.from("orange");
        List<OfferResult> offersApplied = Arrays.asList(
                new OfferResult(apple.getPrice(), new BuyOneGetOneFreeAppleOffer().getDescription()),
                new OfferResult(Double.valueOf("0"), new ThreeForThePriceOfTwoOrangesOffer().getDescription()));
        CheckoutResponse checkoutResponse = new CheckoutResponse(Arrays.asList(apple, apple, orange),
                apple.getPrice() + orange.getPrice(), offersApplied);
        ObjectMapper jsonMapper = new ObjectMapper();
        this.mockMvc.perform(
                post("/api/checkout/1.0")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(testShoppingCart)))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonMapper.writeValueAsString(checkoutResponse)));
    }

    @Test
    public void threeForThePriceOfTwoOrangesCheckoutTest() throws Exception {
        ShoppingCart testShoppingCart = new ShoppingCart();
        testShoppingCart.setItems(new String[]{"orange", "orange", "orange"});
        Item apple = ItemFactory.from("apple");
        Item orange = ItemFactory.from("orange");
        List<OfferResult> offersApplied = Arrays.asList(
                new OfferResult(Double.valueOf("0.0"), new BuyOneGetOneFreeAppleOffer().getDescription()),
                new OfferResult(orange.getPrice(), new ThreeForThePriceOfTwoOrangesOffer().getDescription()));
        CheckoutResponse checkoutResponse = new CheckoutResponse(Arrays.asList(orange, orange, orange),
                 orange.getPrice() * 2, offersApplied);
        ObjectMapper jsonMapper = new ObjectMapper();
        this.mockMvc.perform(
                post("/api/checkout/1.0")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(testShoppingCart)))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonMapper.writeValueAsString(checkoutResponse)));
    }

    @Test
    public void invalidItemTest() throws Exception {
        ShoppingCart testShoppingCart = new ShoppingCart();
        testShoppingCart.setItems(new String[]{"apple", "apple", "orange", "invalid"});
        ObjectMapper jsonMapper = new ObjectMapper();
        this.mockMvc.perform(
                post("/api/checkout/1.0")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(testShoppingCart)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void emptyCartTest() throws Exception {
        ShoppingCart testShoppingCart = new ShoppingCart();
        testShoppingCart.setItems(new String[]{});
        ObjectMapper jsonMapper = new ObjectMapper();
        this.mockMvc.perform(
                post("/api/checkout/1.0")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonMapper.writeValueAsString(testShoppingCart)))
                .andExpect(status().isBadRequest());
    }
}
