package com.ltg.applesnoranges.checkoutservice.util;

import com.ltg.applesnoranges.checkoutservice.domain.Item;
import com.ltg.applesnoranges.checkoutservice.vo.CheckoutResponse;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class PriceCalculatorTest {

    private PriceCalculator priceCalculator;

    @Before
    public void setUp() {
        priceCalculator = new PriceCalculatorImpl();
    }

    @After
    public void tearDown() {
        priceCalculator = null;
    }

    @Test
    public void applePriceCalculatorTest() throws Exception {
        Item item = ItemFactory.from("apple");
        List<Item> items = Arrays.asList(item);
        CheckoutResponse checkoutResponse = priceCalculator.calculatePrice(items);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(1, checkoutResponse.getItems().size());
        Assert.assertEquals(item.getPrice()* items.size(),
                checkoutResponse.getTotalPrice(), 0.0);
    }

    @Test
    public void applesPriceCalculatorTest() throws Exception {
        Item item = ItemFactory.from("apple");
        List<Item> items = Arrays.asList(item, item);
        CheckoutResponse checkoutResponse = priceCalculator.calculatePrice(items);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(2, checkoutResponse.getItems().size());
        Assert.assertEquals(item.getPrice()* items.size(),
                checkoutResponse.getTotalPrice(), 0.0);
    }

    @Test
    public void orangePriceCalculatorTest() throws Exception {
        Item item = ItemFactory.from("orange");
        List<Item> items = Arrays.asList(item);
        CheckoutResponse checkoutResponse = priceCalculator.calculatePrice(items);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(1, checkoutResponse.getItems().size());
        Assert.assertEquals(item.getPrice()* items.size(),
                checkoutResponse.getTotalPrice(), 0.0);
    }

    @Test
    public void orangesPriceCalculatorTest() throws Exception {
        Item item = ItemFactory.from("orange");
        List<Item> items = Arrays.asList(item, item);
        CheckoutResponse checkoutResponse = priceCalculator.calculatePrice(items);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(2, checkoutResponse.getItems().size());
        Assert.assertEquals(item.getPrice()* items.size(),
                checkoutResponse.getTotalPrice(), 0.0);
    }

    @Test
    public void appleAndOrangePriceCalculatorTest() throws Exception {
        Item apple = ItemFactory.from("apple");
        Item orange = ItemFactory.from("orange");
        List<Item> items = Arrays.asList(apple, orange);
        CheckoutResponse checkoutResponse = priceCalculator.calculatePrice(items);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(2, checkoutResponse.getItems().size());
        Assert.assertEquals(apple.getPrice() + orange.getPrice(),
                checkoutResponse.getTotalPrice(), 0.0);
    }

    @Test
    public void applesAndOrangesPriceCalculatorTest() throws Exception {
        Item apple = ItemFactory.from("apple");
        Item orange = ItemFactory.from("orange");
        List<Item> items = Arrays.asList(apple, apple, orange, apple);
        CheckoutResponse checkoutResponse = priceCalculator.calculatePrice(items);
        Assert.assertNotNull(checkoutResponse);
        Assert.assertEquals(4, checkoutResponse.getItems().size());
        Assert.assertEquals(((apple.getPrice() * 3) + orange.getPrice()),
                checkoutResponse.getTotalPrice(), 0.0);
    }

}
