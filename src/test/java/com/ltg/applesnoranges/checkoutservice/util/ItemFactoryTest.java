package com.ltg.applesnoranges.checkoutservice.util;


import com.ltg.applesnoranges.checkoutservice.domain.Item;
import org.junit.Assert;
import org.junit.Test;

public class ItemFactoryTest {

    @Test
    public void appleTest() throws Exception {
        Item apple = ItemFactory.from("apple");
        Assert.assertNotNull(apple);
        Assert.assertEquals("apple", apple.getName().toLowerCase());
        Assert.assertEquals(Double.valueOf("0.6"), apple.getPrice());
    }

    @Test
    public void orangeTest() throws Exception {
        Item orange = ItemFactory.from("orange");
        Assert.assertNotNull(orange);
        Assert.assertEquals("orange", orange.getName().toLowerCase());
        Assert.assertEquals(Double.valueOf("0.25"), orange.getPrice());
    }

    @Test(expected = RuntimeException.class)
    public void invalidItemTest() throws Exception {
        ItemFactory.from("invalid");
    }

}
